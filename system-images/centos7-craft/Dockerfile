# centos7 with modern c++ toolchain
FROM centos/devtoolset-7-toolchain-centos7

LABEL Description="KDE Centos 7 Base for use with Craft"
LABEL maintainer="KDE Sysadmin <sysadmin@kde.org>"

# Start off as root
USER root

RUN yum update -y && \
    # install some core tools
    yum install -y which cmake git python3 && \
    yum groupinstall -y "Development Tools" && \
    yum install -y \
        # install Qt build dependencies, see: https://wiki.qt.io/Building_Qt_5_from_Git
        # qtbase
        libxcb libxcb-devel xcb-util xcb-util-devel mesa-libGL-devel libxkbcommon-devel libudev-devel \
        xcb-util-keysyms-devel libxkbcommon-x11-devel libinput-devel xcb-util-image-devel \
        mesa-libgbm-devel xcb-util-wm-devel xcb-util-renderutil-devel libSM-devel \
        mariadb-devel postgresql-devel unixODBC-devel pcre2-devel \
        # gtk widget theme support
        gtk3-devel \
        # qtwebengine
        freetype-devel fontconfig-devel pciutils-devel nss-devel nspr-devel ninja-build gperf \
        cups-devel pulseaudio-libs-devel libcap-devel alsa-lib-devel bison libXrandr-devel \
        libXcomposite-devel libXcursor-devel libXtst-devel dbus-devel fontconfig-devel \
        alsa-lib-devel rh-nodejs12-nodejs rh-nodejs12-nodejs-devel \
        # qtmultimedia
        pulseaudio-libs-devel alsa-lib-devel gstreamer1-devel gstreamer1-plugins-base-devel wayland-devel \
        # qtwebkit
        ruby \
        # qtspeech
        speech-dispatcher-devel \
        # kshimgen
        glibc-static \
        # kfilemetadata
        libattr-devel \
        # debugging
        nano \
        # appimages
        fuse fuse-libs \
        # CI support
        openssh-server java-1.8.0-openjdk-headless openjdk-8-jre-headless \
        python2.7 python3.6 python3-yaml python3-lxml python3-paramiko \
        # kdev-python
        python3-devel \
        # latest devtoolset
        devtoolset-9

# use gold for linking
RUN alternatives --install /opt/rh/devtoolset-9/root/usr/bin/ld ld-9 /opt/rh/devtoolset-9/root/usr/bin/ld.gold 1

# use latest devtoolset
COPY scl_enable.sh /usr/local/bin/scl_enable.sh
ENV BASH_ENV="/usr/local/bin/scl_enable.sh" \
    ENV="/usr/local/bin/scl_enable.sh" \
    PROMPT_COMMAND=". /usr/local/bin/scl_enable.sh"

# patch header to make it standalone, required for poppler compilation
RUN sed -i '/#define _HASHT_H_/a #include <prtypes.h>' /usr/include/nss3/hasht.h

# Setup a user account for everything else to be done under
RUN useradd -d /home/appimage/ -u 1000 --user-group --create-home -G video appimage
# Make sure SSHD will be able to startup
RUN mkdir /var/run/sshd/
# Get locales in order
RUN localedef -c -i en_US -f UTF-8 en_US.UTF-8

# let sshd-keygen work
COPY etc_rc_d_init_d_functions.sh /etc/rc.d/init.d/functions
RUN sshd-keygen

# let git work
RUN git config --system merge.defaultToUpstream true

# We want to run SSHD so that Jenkins can remotely connect to this container
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
