# FreeBSD 12 Jenkins Worker Installation

> This document describes how to set up a FreeBSD node
> in the KDE CI network. This documentation **might** be
> generic enough to use for FreeBSD 13 installation when
> that comes out.

## Base System

 - Get the [FreeBSD 12.0-R][fbsd-12] image and install it.
 - Add the following in `/etc/sysctl.conf`:
   ```
# ASAN performance mitigations
kern.proc_vmmap_skip_resident_count=1
# Akonadi recommended settings
net.local.stream.recvspace=65536
net.local.stream.sendspace=65536
```
 - Install *pkg(8)* with `pkg install pkg`.
 - Update the base system (as needed) with `freebsd-update`.

## Dependencies

 - Switch over the package repository (step 1), 
   add `/usr/local/etc/pkg/repos/FreeBSD.conf` with this content:
   ```
FreeBSD: {
enabled: no
}
```
 - Switch over the package repository (step 2), 
   add `/usr/local/etc/pkg/repos/ci_repos.conf` with this content:
   ```
ci_pkgs: {
  url: "pkg+https://build-artifacts.kde.org/freebsdrepo/",
  mirror_type: "SRV",
  signature_type: "none",
  enabled: yes,
  priority: 10
}
```
 - Install the [dependency packages][fbsd-deps] with 
   `pkg install $( cat package-list )`.
 - Enable dbus, `sysrc enable_dbus=YES`.
 - Run `dbus-uuidgen`.
 - Create `/usr/local/etc/libmap.d/gfortran.conf` with this content:
   ```
libgcc_s.so.1 /usr/local/lib/gcc8/libgcc_s.so.1
```



[fbsd-12]: https://www.freebsd.org/releases/12.0R/announce.html
[fbsd-deps]: FreeBSD-12-packages.txt
