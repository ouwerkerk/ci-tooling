// Read the contents of the gathered-jobs.json file a step created for us previously
def jobsToParse = readFileFromWorkspace('custom-jobs/known-jobs.json')
def knownJobs = new groovy.json.JsonSlurper().parseText( jobsToParse )

// Iterate over all of the known jobs and create them!
knownJobs.each {
	// Save our job name for later
	def jobName = "${it.name}"

	// Read in the necessary Pipeline script
	def pipelineScript = readFileFromWorkspace("custom-jobs/${it.name}.pipeline")

	// Actually create the job now
	pipelineJob( jobName ) {
		properties {
			// We don't want to keep build results forever
			// We'll set it to keep the last 10 builds and discard everything else
			buildDiscarder {
				strategy {
					logRotator {
						numToKeepStr("25")
						daysToKeepStr('')
						artifactDaysToKeepStr('')
						artifactNumToKeepStr('')
					}
				}
			}
			// We don't want to be building the same project more than once
			// This is to prevent one project hogging resources
			// And also has a practical component as otherwise an older build could finish afterwards and upload old build results
			disableConcurrentBuilds()
		}
		triggers {
			// We want to enable SCM Polling so that git.kde.org can tell Jenkins to look for changes
			// At the same time, we don't want Jenkins scanning for changes, so set the Polling specification to be empty so nothing gets scheduled
			pollSCM {
				scmpoll_spec('')
				ignorePostCommitHooks(false)
			}
		}
		// This is where the Pipeline script actually happens :)
		definition {
			cps {
				script( pipelineScript )
				sandbox()
			}
		}
	}
}
